<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class WelcomeController extends AbstractController
{


    public function index(): Response
    {

        return new Response('hi,i am a welcome controller');
    }

}